package com.clantat.Strings;
/*
Given a string, look for a mirror image (backwards) string at both the beginning and end of the given string.
In other words, zero or more characters at the very begining of the given string, and at the very end of the string in reverse order (possibly overlapping).
For example, the string "abXYZba" has the mirror end "ab".


mirrorEnds("abXYZba") → "ab"
mirrorEnds("abca") → "a"
mirrorEnds("aba") → "aba"
https://codingbat.com/prob/p139411
 */
public class String3_mirrorEnds {
    public String mirrorEnds(String string) {
        String level="";
        for (int i = 0,j=string.length(); i<string.length() ; i++,j--) {
            if(string.charAt(i)==string.charAt(j-1)){
                level=string.substring(0,i+1);
            }
            else break;
        }
        string=level;
        return string;
    }
}
