package com.clantat.Strings;
/*
Look for patterns like "zip" and "zap" in the string -- length-3, starting with 'z' and ending with 'p'. Return a string where for all such words, the middle letter is gone, so "zipXzap" yields "zpXzp".


zipZap("zipXzap") → "zpXzp"
zipZap("zopzop") → "zpzp"
zipZap("zzzopzop") → "zzzpzp"
https://codingbat.com/prob/p180759
 */
public class String2_zipZap {
    public String zipZap(String str) {
        int before=0;
        String strBefore="";
        int after=0;
        String strAfter="";
        for (int i = 0; i <str.length() ; i++) {
            before = str.indexOf("z",i);
            if(before!=-1) {
                strBefore = str.substring(0, before + 1);
            }
            after = str.indexOf("p",i+2);
            if(after!=-1){
                strAfter = str.substring(after);
            }
            if (before + 2 == after) {
                str = strBefore + strAfter;
            }
        }
        return str;
    }
}
