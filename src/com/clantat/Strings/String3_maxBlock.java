package com.clantat.Strings;
/*

Given a string, return the length of the largest "block" in the string. A block is a run of adjacent chars that are the same.


maxBlock("hoopla") → 2
maxBlock("abbCCCddBBBxx") → 3
maxBlock("") → 0
https://codingbat.com/prob/p179479
 */
public class String3_maxBlock {
    public int maxBlock(String str) {
        int max=1;
        int length=0;
        for (int i = 1; i <str.length() ; i++) {
            if(str.charAt(i-1)==str.charAt(i)){
                max++;
            }
            else {
            max=1;
            }
            if(length<max) {
                length = max;
            }
        }
        return length;
    }
}
