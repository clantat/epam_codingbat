package com.clantat.Strings;
/*
Given a string, return a new string made of 3 copies of the last 2 chars of the original string. The string length will be at least 2.

extraEnd("Hello") → "lololo"
extraEnd("ab") → "ababab"
extraEnd("Hi") → "HiHiHi"
https://codingbat.com/prob/p108853
 */
public class String1_extraEnd {
    public String extraEnd(String str) {
        str=str.substring(str.length()-2);
        str=str+str+str;
        return str;
    }
}
